package me.jaxos.bobay;

import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Cow;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener
{
    public void onEnable() {
        this.getLogger().info("Bò bay đang chạy !)");
        this.getServer().getPluginManager().registerEvents((Listener)this, (Plugin)this);
    }
    
    public void onDisable() {
        this.getLogger().info("Bò bay đã tắt !");
    }
    
    @EventHandler
    public void onDamge(final EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Cow && event.getDamager() instanceof Player) {
            final Player CowPuncher = (Player)event.getDamager();
            final LivingEntity CowHit = (LivingEntity)event.getEntity();
            if (CowPuncher.hasPermission("bobay.sudung")) {
                final PotionEffect potion = new PotionEffect(PotionEffectType.LEVITATION, 200, 1);
                CowHit.addPotionEffect(potion, true);
            }
        }
    }
}